**# Usando:** 
	*## Lenguajes:* Java, HTML5, CSS3
	*## Plataforma:* Eclipse, Windows
	*## Herramientas:* SublimeText, Git
	*## Técnicas:* TDD

**# Evitar:**
	*## Lenguajes:* ActionScript 
	*## Plataforma:* CMS
	*## Herramientas:*
	*## Técnicas:* Mal uso de nombres 

**# Evaluar:**
	*## Lenguajes:* AngularJS 2, Python
	*## Plataforma:* Codonvy o C9
	*## Herramientas:*
	*## Técnicas:* Desarrollo en la nube

**# Probar:**
	*## Lenguajes:* 
	*## Plataforma:* PostgreSQL
	*## Herramientas:*
	*## Técnicas:* BDD

